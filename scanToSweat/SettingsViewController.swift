//
//  SettingsViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 24.11.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var cellsArray:[UITableViewCell] = []
    let centerButtonSwitch = UISwitch()
    weak var containerView:SnapContainerViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let maskPath = UIBezierPath(roundedRect: tableViewContainer.bounds,
                                    byRoundingCorners: [.bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        tableViewContainer.layer.mask = shape
        
        
        containerView = self.parent?.parent as? SnapContainerViewController
        
        centerButtonSwitch.isOn = containerView.whereAmIViewController.openSearchFunction
        centerButtonSwitch.addTarget(self, action: #selector(self.switchOpenSearchOnMiddlebutton), for: .valueChanged)
        
        
        
        
        let centerButtonCell = UITableViewCell()
        centerButtonCell.accessoryView = centerButtonSwitch
        centerButtonCell.textLabel?.text = NSLocalizedString("Öffne Suche mit Mittelbutton", comment: "")
        cellsArray.append(centerButtonCell)
        
        let impressumCell = UITableViewCell()
        impressumCell.accessoryType = .disclosureIndicator
        impressumCell.textLabel?.text = NSLocalizedString("Impressum", comment: "")

        cellsArray.append(impressumCell)
    }
    
    override func viewDidAppear(_ animated: Bool){
        DispatchQueue.main.async {
            self.containerView?.showIndicators()
        }
        
        // deselect the selected row if any
        let selectedRow: IndexPath? = tableView.indexPathForSelectedRow
        if let selectedRowNotNill = selectedRow {
            tableView.deselectRow(at: selectedRowNotNill, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tabellenfunktionen
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellsArray[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            containerView.setSearchFunction(!centerButtonSwitch.isOn)
            centerButtonSwitch.setOn(!centerButtonSwitch.isOn, animated: true)
            switchOpenSearchOnMiddlebutton(sender: centerButtonSwitch)
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        if(indexPath.row == 1){
            containerView?.hideIndicators()
            performSegue(withIdentifier: "impressumSegue", sender: self)
        }
    }
    
    //MARK: Einstellungsfunktionen
    @objc func switchOpenSearchOnMiddlebutton(sender:UISwitch!) {
       // print(sender.isOn)
        UserDefaults.standard.setValue(sender.isOn, forKey: "shouldOpenSearch")
        containerView.setSearchFunction(sender.isOn)
    }

    
}
