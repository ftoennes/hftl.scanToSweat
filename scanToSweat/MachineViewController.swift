//
//  MachineViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 24.11.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class MachineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var addSetBtn: UIButton!
    @IBOutlet weak var machineIdLabel: UILabel!
    @IBOutlet weak var machineNameLabel: UILabel!
    @IBOutlet weak var exerciseNameLabel: UILabel!
    @IBOutlet weak var exerciseDescription: UILabel!
    @IBOutlet weak var expandDescBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var sets: [Sets] = Array()
    var exercise: Exercise?
    var training: Training?

    var machineId: String!
    
    //Core data
    let context = (UIApplication.shared.delegate as? AppDelegate)!.persistentContainer.viewContext


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //prevents white space in machineView
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.tableView.allowsMultipleSelectionDuringEditing = false
    }
    
    func updateTableLayout(){
        UIView.animate(withDuration: 0.1, animations: {
            self.tableView.reloadData()
            self.tableViewHeightConstraint.constant = CGFloat(self.sets.count * 55)
            self.tableView.layoutIfNeeded()
            self.tableView.superview?.layoutIfNeeded()
            self.tableView.superview?.superview?.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            
        })
    }

    
    @IBAction func close() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let navigationController = appDelegate?.snapContainer?.rightVc as? UINavigationController
        let snapController: SnapContainerViewController? = appDelegate?.snapContainer as SnapContainerViewController?
        snapController?.hideWhereAmI = true
        let rightViewController = navigationController?.viewControllers[0] as? RightViewController
        rightViewController?.reloadData()
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        machineIdLabel.text = machineId
        
        let machineData: Bool = getDataFromDatabase()
        if (machineData == false) {
            getDataFromBackend()
        }
        
        //sucht nach heutigem Training und lädt es in tableView
        loadSets()
    }
    
    func getDataFromDatabase() -> Bool {
        let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
        request.predicate = NSPredicate(format: "id == %@", machineId)
        do {
            let dbExercise = try context.fetch(request)
            self.exercise = dbExercise.first
            DispatchQueue.main.async {
                self.machineNameLabel.text = dbExercise.first?.geraeteId
                self.exerciseNameLabel.text = dbExercise.first?.name
                self.exerciseDescription.text = dbExercise.first?.desc
                if (!self.exerciseDescription.isTruncated) {
                    self.expandDescBtn.isHidden = true
                }
            }
            return true
        } catch {
            print("error while fetching")
            return false
        }
    }
    
    func getDataFromBackend() {
        let uri = "https://lawnetix.org:81/api/exercise/?id=" + machineId!
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, _) in
            let nsUrlResponse = response as? HTTPURLResponse
            
            if nsUrlResponse?.statusCode == 200 {
                let data = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                let machineName = data.components(separatedBy: ";")[3]
                let exerciseName = data.components(separatedBy: ";")[0]
                DispatchQueue.main.async {
                    self.machineNameLabel.text = machineName
                    self.exerciseNameLabel.text = exerciseName
                }
            } else if nsUrlResponse?.statusCode == 404 {
                DispatchQueue.main.async {
                    self.machineNameLabel.text = "404"
                    self.exerciseNameLabel.text = "404"
                }
            }
        }
        task.resume()
    }

    
    // MARK: - Navigation

 
    @IBAction func expandDescAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.1) {
            if self.exerciseDescription.numberOfLines == 0 {
                self.exerciseDescription.numberOfLines = 2
                self.expandDescBtn.setTitle(NSLocalizedString("mehr anzeigen", comment: ""), for: .normal)
            } else {
                self.exerciseDescription.numberOfLines = 0
                self.expandDescBtn.setTitle(NSLocalizedString("weniger anzeigen", comment: ""), for: .normal)
            }
            
            self.exerciseDescription.superview?.layoutIfNeeded()
            self.exerciseDescription.superview?.superview?.layoutIfNeeded()
            self.exerciseDescription.superview?.superview?.superview?.layoutIfNeeded()
        }
    }
 
    
    // MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "SetRow" + String(indexPath.row))
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetRow" + String(indexPath.row)) as? TableViewCell
        cell?.scrollView = self.scrollView
        cell?.set = sets[indexPath.row]
        cell?.setLabels()
        cell?.setNumberLabel.text = String(indexPath.row+1)
        
        addSetBtn.setTitle(NSLocalizedString("Set hinzufügen", comment: ""), for: .normal)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle,
                   forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            tableView.beginUpdates()
            
            context.delete(sets[indexPath.row])
            do { try context.save() } catch {}
            
                self.sets.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            tableView.endUpdates()
            updateTableLayout()
        }
    }
    
    // MARK: - Set Funktionen
    
    @IBAction func addSet(sender: Any?) {
        
        let set = Sets(context: context)
        set.machine = exercise
        set.reps = 12
        set.training = training
        set.weight = 15
        
        var trainingSets = training!.sets?.allObjects as? [Sets]
        trainingSets?.append(set)
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        addSetBtn.setTitle(NSLocalizedString("Set hinzufügen", comment: ""), for: .normal)
        loadSets()
        tableView.reloadData()
        updateTableLayout()
    }
    
    func getTraining() -> Training? {
        let trainingFetchRequest: NSFetchRequest<Training> = Training.fetchRequest()
        
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        let dateFrom = calendar.startOfDay(for: Date())
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += 1
        let dateTo = calendar.date(from: components)!
        
        trainingFetchRequest.predicate = NSPredicate(format: "(%@ <= date) AND (date < %@)", dateFrom as NSDate, dateTo as NSDate)
        do {
            let dbTraining = try context.fetch(trainingFetchRequest)
            self.training = dbTraining.last
            return(dbTraining.last)
        } catch {
            print("error while fetching")
            return nil
        }
    }
    
    func loadSets() {
        if exercise != nil{
            if let training: Training = getTraining() {
                let setFetchRequest: NSFetchRequest<Sets> = Sets.fetchRequest()
                setFetchRequest.predicate = NSPredicate(format: "training == %@ && machine == %@", training, exercise!)
                do {
                    let dbSets = try context.fetch(setFetchRequest)
                    sets = dbSets
                    tableView.reloadData()
                    updateTableLayout()
                } catch {
                    print("error while fetching")
                }
            } else {
                createTraining()
            }
        }
    }
    
    func createTraining() {
        let training = Training(context: context)
        training.date = Date()
        self.training = training
    }
    
    
    // MARK: - hide Keyboard
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - helper-Methoden
  
    func addGradientOverlay() {
        let gradientLayer: CAGradientLayer! = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [
            UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0),
            UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.95),
            UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.5)
            ].map { $0.cgColor }
        gradientLayer.locations = [0.0, 0.8, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
}


//MARK: - UILabel-Erweiterung

extension UILabel {
    
    var isTruncated: Bool {
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}
