//
//  UITableViewCell.swift
//  scanToSweat
//
//  Created by Leon Schlote on 14.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class ExerciseDetailSetTableViewCell: UITableViewCell {
    @IBOutlet weak var setNumberLabel: UILabel!
    @IBOutlet weak var repetitionsLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
}
