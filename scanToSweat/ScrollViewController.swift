//
//  MiddleScrollViewController.swift
//  SnapchatSwipeView
//
//

import UIKit

protocol ScrollViewControllerDelegate {
    func outerScrollViewShouldScroll() -> Bool
}

class ScrollViewController: UIViewController, SnapContainerViewControllerDelegate, UIScrollViewDelegate {
    var topVc: UIViewController!
    var middleVc: UIViewController!
    var bottomVc: UIViewController!
    var scrollView: UIScrollView!
    
    class func verticalScrollVcWith(middleVc: UIViewController,
                                    topVc: UIViewController?=nil,
                                    bottomVc: UIViewController?=nil) -> ScrollViewController {
        let middleScrollVc = ScrollViewController()
        
        middleScrollVc.topVc = topVc
        middleScrollVc.middleVc = middleVc
        middleScrollVc.bottomVc = bottomVc
        
        return middleScrollVc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view:
        setupScrollView()
    }
    
    func setupScrollView() {
        scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = true
        scrollView.bounces = false
        scrollView.delegate = self

        let view = (
            x: self.view.bounds.origin.x,
            y: self.view.bounds.origin.y,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        )
        
        scrollView.frame = CGRect(x: view.x, y: view.y, width: view.width, height: view.height)
        self.view.addSubview(scrollView)
        
        let scrollWidth: CGFloat  = view.width
        var scrollHeight: CGFloat
        
      
        scrollHeight  = view.height
        middleVc.view.frame = CGRect(x: 0, y: 0, width: view.width, height: view.height)
        
        addChildViewController(middleVc)
        scrollView.addSubview(middleVc.view)
        middleVc.didMove(toParentViewController: self)

        
        scrollView.contentSize = CGSize(width: scrollWidth, height: scrollHeight)
    }
    
    // MARK: - SnapContainerViewControllerDelegate Methods
    
    func outerScrollViewShouldScroll() -> Bool {
        if scrollView.contentOffset.y < middleVc.view.frame.origin.y || scrollView.contentOffset.y > 2*middleVc.view.frame.origin.y {
            return false
        } else {
            return true
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //disable camera when not on middleViewController
        if (scrollView.bounds.intersection((bottomVc.view.frame)) == bottomVc.view.frame) {
            self.middleVc.viewDidDisappear(false)
        } else if (scrollView.bounds.intersection((middleVc.view.frame)) == middleVc.view.frame) {
            self.middleVc.viewDidAppear(false)
        }
    }
    
    // MARK: - scroll programmatically
    func scrollToBottomView() {
        scrollView?.setContentOffset(CGPoint(x: (bottomVc?.view.frame.origin.x)!, y: (bottomVc?.view.frame.origin.y)!), animated: true)
    }

    
}
