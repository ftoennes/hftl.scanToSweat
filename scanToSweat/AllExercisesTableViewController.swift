//
//  AllExercisesTableViewController.swift
//  scanToSweat
//
//  Created by Leon Schlote on 13.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class AllExercisesTableViewController: UITableViewController {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var exerciseArray:[Exercise] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
        
        let sort = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sort]
        
        do {
            let fetchedExercises = try context.fetch(request)
            DispatchQueue.main.async {
                self.exerciseArray = fetchedExercises
                self.tableView.reloadData()
            }
        } catch {
            print("error while fetching Trainings")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // deselect the selected row if any
        let selectedRow: IndexPath? = tableView.indexPathForSelectedRow
        if let selectedRowNotNill = selectedRow {
            tableView.deselectRow(at: selectedRowNotNill, animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if !(self.navigationController?.visibleViewController is AllExercisesDetailTableViewController) {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.snapContainer?.hideWhereAmI = false
            appDelegate?.snapContainer?.showIndicators()
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return exerciseArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = UITableViewCell() //tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

         // Configure the cell...
         let name = exerciseArray[indexPath.row].name!
         let geraet = exerciseArray[indexPath.row].geraeteId!
        
        cell.textLabel?.text = name + " ("+geraet+")"
        cell.accessoryType = .disclosureIndicator*/
        
        // Configure the cell...
        let name = exerciseArray[indexPath.row].name!
        let geraet = exerciseArray[indexPath.row].geraeteId!
        
        tableView.register(UINib(nibName: "MachineCell", bundle: nil), forCellReuseIdentifier: "MachineRow" + String(indexPath.row))
        let cell = tableView.dequeueReusableCell(withIdentifier: "MachineRow" + String(indexPath.row)) as? MachineCell
        cell?.idLabel.text = exerciseArray[indexPath.row].id!
        cell?.label.text = name + " ("+geraet+")"
        
        return cell!
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "allExercisesDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Create a variable that you want to send
       // var newProgramVar = Program(category: "Some", name: "Text")
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! AllExercisesDetailTableViewController
        //print(destinationVC)
        let selectedIndexPath: IndexPath? = tableView.indexPathForSelectedRow
        if let selectedIndexPathNotNil = selectedIndexPath {
            
            destinationVC.exercise = exerciseArray[selectedIndexPathNotNil.row]
        }
        
    }
}
