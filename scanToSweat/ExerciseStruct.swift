struct ExerciseStruct : Codable {
    let id: String
    let name: String
    let muskel: String
    let bildId: String
    let geraeteId: String
    let desc: String
}
