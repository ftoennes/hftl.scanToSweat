//
//  AllExercisesDetailTableViewController.swift
//  scanToSweat
//
//  Created by Leon Schlote on 14.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class AllExercisesDetailTableViewController: UITableViewController {

    var exercise: Exercise?
    //var trainings: [Training] = []
    var exerciseSetsData : [exerciseContainer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        //flush prevously loaded data
        exerciseSetsData.removeAll()
        
        
        let name = exercise!.name!
        let geraet = exercise!.geraeteId!
        navigationItem.title = name + " (" + geraet + ")"
        
        
        let request: NSFetchRequest<Training> = Training.fetchRequest()
        request.predicate = NSPredicate(format: "ANY sets IN %@", exercise!.set! )
        let sort = NSSortDescriptor(key: "date", ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            let fetchedTrainings = try context.fetch(request)
            DispatchQueue.main.async {
                
                for training in fetchedTrainings{
                    var tableCellData = exerciseContainer(datum: training.date!, uebungsSets: [])
                    for set in training.sets?.allObjects as!  [Sets]{
                        if set.machine?.id == self.exercise!.id{
                            tableCellData.uebungsSets.append(set)
                        }
                    }
                    self.exerciseSetsData.append(tableCellData)
                }
                
                self.tableView.reloadData()
            }
        } catch {
            print("error while fetching Trainings")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Tableview Funktionen

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseSetsData.count+1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < exerciseSetsData.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseDetailCell", for: indexPath) as! ExerciseDetailCell
            cell.setDatum(exerciseSetsData[indexPath.row].datum)
            cell.setSets(exerciseSetsData[indexPath.row].uebungsSets)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CenterButtonCell", for: indexPath) as! CenterButtonCell
            cell.machineID = exercise!.id!
            cell.table = self
            return cell
            
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if(indexPath.row < exerciseSetsData.count){
            let setCount = exerciseSetsData[indexPath.row].uebungsSets.count
            return CGFloat(setCount * 25 + 30)
        }else{
            return 60.0
            
        }
    }

}

struct exerciseContainer{
    var datum:Date
    var uebungsSets:[Sets]
}
