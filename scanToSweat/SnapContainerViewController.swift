//
//  ContainerViewController.swift
//  SnapchatSwipeView
//
//

import UIKit

protocol SnapContainerViewControllerDelegate {
    func outerScrollViewShouldScroll() -> Bool
}

class SnapContainerViewController: UIViewController, UIScrollViewDelegate {
    
    var whereAmIViewController: WhereAmIViewController!
    var leftVc: UIViewController!
    var middleVc: SearchViewController!
    var rightVc: UIViewController!
    
    var overlayView:UIView?
    
    var hideWhereAmI: Bool = true

    
   // var backgroundCameraVc: CameraViewController!
    
    var cameraVc: CameraViewController!
    
    var directionLockDisabled: Bool!
    
    var horizontalViews = [UIViewController]()
    var veritcalViews = [UIViewController]()
    
    var initialContentOffset = CGPoint() // scrollView initial offset
    var middleVertScrollVc: ScrollViewController!
    var scrollView: UIScrollView!
    var delegate: SnapContainerViewControllerDelegate?
    
    var triggered: Bool = false //needed to activate/deactive camera only once while scrolling
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupVerticalScrollView()
        setupHorizontalScrollView()
        middleVertScrollVc.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        //backgroundCameraVc = CameraViewController.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        cameraVc = storyboard.instantiateViewController(withIdentifier: "camera") as! CameraViewController
        
        
        cameraVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.insertSubview(cameraVc.view, at: 0)
    
        overlayView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        overlayView!.backgroundColor = UIColor(rgb: 0x4D4D52)
        overlayView!.alpha = 0
        self.view.insertSubview(overlayView!, at: 1)
        
        //setup whereAmI-View
        whereAmIViewController = WhereAmIViewController(nibName: "WhereAmIViewController", bundle: nil)
        whereAmIViewController.scrollView = self.scrollView
        whereAmIViewController.searchView = self.middleVc
        let whereAmIView = whereAmIViewController.view!
        
        whereAmIView.frame = CGRect(x: 0, y: self.view.frame.height-50, width: self.view.frame.width, height: 40)
        self.view.addSubview(whereAmIView)
        
        //read settings from plist
        // open keyboard on buttonPress
        if (UserDefaults.standard.bool(forKey: "shouldOpenSearch") == true) {
            setSearchFunction(true)
        }
    }
    
    func setupVerticalScrollView() {
        middleVertScrollVc = ScrollViewController.verticalScrollVcWith(middleVc: middleVc,
                                                                               topVc: nil,
                                                                               bottomVc: nil)
        delegate = middleVertScrollVc
    }
    
    class func containerViewWith(_ leftVC: UIViewController,
                                 middleVC: SearchViewController,
                                 rightVC: UIViewController,
                                 directionLockDisabled: Bool?=false) -> SnapContainerViewController {
        let container = SnapContainerViewController()
        
        container.directionLockDisabled = directionLockDisabled
        
        container.leftVc = leftVC
        container.middleVc = middleVC
        container.rightVc = rightVC
        return container
    }
    
    func setSearchFunction(_ shouldOpenSearch:Bool){
        whereAmIViewController.openSearchFunction = shouldOpenSearch
    }
    
    func setupHorizontalScrollView() {
        scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        
        let view = (
            x: self.view.bounds.origin.x,
            y: self.view.bounds.origin.y,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        )
        
        scrollView.frame = CGRect(x: view.x,
                                  y: view.y,
                                  width: view.width,
                                  height: view.height
        )
        
        self.view.addSubview(scrollView)
        
        let scrollWidth  = 3 * view.width
        let scrollHeight  = view.height
        scrollView.contentSize = CGSize(width: scrollWidth, height: scrollHeight)
        
        leftVc.view.frame = CGRect(x: 0,
                                   y: 0,
                                   width: view.width,
                                   height: view.height
        )
        
        middleVertScrollVc.view.frame = CGRect(x: view.width,
                                               y: 0,
                                               width: view.width,
                                               height: view.height
        )
        
        rightVc.view.frame = CGRect(x: 2 * view.width,
                                    y: 0,
                                    width: view.width,
                                    height: view.height
        )
        
        addChildViewController(leftVc)
        addChildViewController(middleVertScrollVc)
        addChildViewController(rightVc)
        
        scrollView.addSubview(leftVc.view)
        scrollView.addSubview(middleVertScrollVc.view)
        scrollView.addSubview(rightVc.view)
        leftVc.didMove(toParentViewController: self)
        middleVertScrollVc.didMove(toParentViewController: self)
        rightVc.didMove(toParentViewController: self)

        scrollView.contentOffset.x = middleVertScrollVc.view.frame.origin.x
        scrollView.delegate = self
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.initialContentOffset = scrollView.contentOffset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if delegate != nil && !delegate!.outerScrollViewShouldScroll() && !directionLockDisabled {
            let newOffset = CGPoint(x: self.initialContentOffset.x, y: self.initialContentOffset.y)
        
            // Setting the new offset to the scrollView makes it behave like a proper
            // directional lock, that allows you to scroll in only one direction at any given time
            self.scrollView!.setContentOffset(newOffset, animated:  false)
        }
        
        //disable camera when not on middleViewController
        /*if (scrollView.bounds.intersection((leftVc.view.frame)) == leftVc.view.frame)
            || (scrollView.bounds.intersection((rightVc.view.frame)) == rightVc.view.frame)
            //|| (scrollView.bounds.intersection((bottomVc?.view.frame)!) == bottomVc?.view.frame)
        {
            middleVc.viewDidDisappear(false)
        } else if (scrollView.bounds.intersection((middleVertScrollVc.view.frame)) == middleVertScrollVc.view.frame) {
            middleVc.viewDidAppear(false)
        }*/
        
        //disable camera when not on middleViewController
        let viewWidth = leftVc.view.frame.width
        
        //TODO: wieder einkommentieren
        if (cameraVc.isRunning && (scrollView.bounds.origin.x == 0 || scrollView.bounds.origin.x == viewWidth*2)) {
            cameraVc.viewDidDisappear(false)
        } else {
            if !triggered && cameraVc.isRunning == false {
                triggered = true
                self.cameraVc.viewDidAppear(false)
            }
        }
 
        if (scrollView.bounds.origin.x == 0) || (scrollView.bounds.origin.x == viewWidth) || (scrollView.bounds.origin.x == viewWidth*2) {
            triggered = false
        }
        
        //Fade Camera Overlay
        let scrollPercentage = (scrollView.contentOffset.x - (scrollView.frame.width))/(scrollView.frame.width-100)
        overlayView!.alpha = scrollPercentage * scrollPercentage
        
        //Move Circle
        whereAmIViewController.circle.frame.origin.x = ((leftVc.view.frame.width/2 - whereAmIViewController.circle.frame.width/2) + ((scrollView.contentOffset.x)/3.303964757709251)) - leftVc.view.frame.width/3.303964757709251
    }
    
    func hideIndicators(){
        self.scrollView.isScrollEnabled  = false
        UIView.animate(withDuration: 0.1, animations: {
            self.whereAmIViewController.view.alpha = 0
        }) { (finished) in
            self.whereAmIViewController.view.isHidden = finished
        }
    }
    
    func showIndicators(){
        self.scrollView.isScrollEnabled  = true
        if( self.whereAmIViewController.view.isHidden == true && hideWhereAmI == false){
        
            self.whereAmIViewController.view.alpha = 0
            self.whereAmIViewController.view.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.whereAmIViewController.view.alpha = 1
            }
        } else {
            hideWhereAmI = true
        }
    }
    
}
