//
//  ImpressumViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 22.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class ImpressumViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.snapContainer?.hideWhereAmI = false
        appDelegate?.snapContainer?.showIndicators()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
