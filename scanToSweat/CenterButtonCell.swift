//
//  CenterButtonCell.swift
//  scanToSweat
//
//  Created by Leon Schlote on 19.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class CenterButtonCell: UITableViewCell {
    weak var table: AllExercisesDetailTableViewController?
    var machineID: String = ""

    @IBAction func openCorrespondingMachineView(_ sender: Any) {
        DispatchQueue.main.async {

            let machineViewController = MachineViewController()
            machineViewController.machineId = self.machineID
            self.table?.present(machineViewController, animated: true, completion: nil)
        }
    }
}
