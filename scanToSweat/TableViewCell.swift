//
//  TableViewCell.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 10.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var setNumberLabel: UILabel!
    @IBOutlet weak var repeatsLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    public var scrollView: UIScrollView!
    public var set: Sets? = nil
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext


    public func setLabels() {
        repeatsLabel.text = String(describing: set?.reps ?? 0)
        weightTextField.text = String(describing: set?.weight ?? 0)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.donePressed))
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0,  width: self.frame.width, height: self.frame.height))
        toolbar.items = [barButton];
        
        weightTextField.inputAccessoryView = toolbar;
    }

    
    @objc func donePressed(_ sender: Any?)  {
        if (weightTextField.text == "") {
            weightTextField.text = "0"
        }
        endWeightEditing()
        weightTextField.endEditing(false)
    }
    
    @IBAction func increaseRepeats() {
        changeRepeats(+1)
    }
    
    @IBAction func decreaseRepeats() {
        changeRepeats(-1)
    }
    
    private func changeRepeats(_ change:Int) {
        var repeats = Int(repeatsLabel.text!)
        repeats = repeats! + change
        repeatsLabel.text = String(describing: repeats ?? 0)
        set?.reps = Int16(repeats!)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    @IBAction func beginWeightEditing() {
        //TODO: Scroll into view
        
        scrollView.contentSize.height += 216  //extend contentSize + keyboard height
    }
    
    @IBAction func endWeightEditing() {
        scrollView.contentSize.height -= 216
        
        weightTextField.text = weightTextField.text?.replacingOccurrences(of: ",", with: ".")
        set?.weight = Float(weightTextField.text!)!
        weightTextField.text = (weightTextField.text)!
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        //TODO: Scroll back view
    }

}
