//
//  TrainingshistorieViewController.swift
//  scanToSweat
//
//  Created by Leon Schlote on 09.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class TrainingshistorieViewController: UITableViewController {
    
    
    var trainingsCellDataArray: [TrainingsCellData] = []
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request: NSFetchRequest<Training> = Training.fetchRequest()
        let sort = NSSortDescriptor(key: "date", ascending: false)
        request.sortDescriptors = [sort]
        
        let asyncRequest = NSAsynchronousFetchRequest(fetchRequest: request){ (fetchedTrainings) -> Void in
            DispatchQueue.main.async { () -> Void in
                let trainings = fetchedTrainings.finalResult!
                
                for training in trainings{
                    
                    var tempSetDict = [String: [Sets]]()
                    
                    for set in training.sets?.allObjects as! [Sets]{
                        let id:String = (set.machine?.id)!
                        //neues Array Initialisieren falls noch nicht getan
                        if tempSetDict[id] == nil {
                            tempSetDict[id] = []
                        }
                        //Daten zu array hinzufügen
                        tempSetDict[id]!.append(set)
                    }
                    
                    let newTrainingsCellData = TrainingsCellData(
                        date: training.date,
                        exerciseSetDictionary: tempSetDict
                    )
                    //neue Daten in Zell-Daten-Array speichern
                    self.trainingsCellDataArray.append(newTrainingsCellData)
                }
                
                //animation beim einfügen der Zellen
                UIView.animate(withDuration: 0.1, animations: {
                    self.tableView.reloadData()
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                })
            }
        }
        
        //Datenbank request ausführen
        do {
            try context.execute(asyncRequest)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.snapContainer?.hideWhereAmI = false
        appDelegate?.snapContainer?.showIndicators()
    }


    // MARK: - Tableview funktionen

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(trainings.count)
        return trainingsCellDataArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingsCell", for: indexPath) as! TrainingsCell
        let data = trainingsCellDataArray[indexPath.row]
        
         //Zelle konfigurieren
        cell.setDatum(data.date)
        cell.setSets(data.exerciseSetDictionary)

        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
       return CGFloat(trainingsCellDataArray[indexPath.row].exerciseSetDictionary.count * 25 + 30)
    }
}

//Custom Type für Trainings-Zellen-Daten
struct TrainingsCellData{
    var date:Date!
    var exerciseSetDictionary:[String:[Sets]]
}
