//
//  WhereAmIViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 24.11.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class WhereAmIViewController: UIViewController {
    
    @IBOutlet weak var circle: UIImageView!
    
    weak var scrollView: UIScrollView!
    weak var searchView: SearchViewController!
    
    let screenSize: CGRect = UIScreen.main.bounds
    var openSearchFunction = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func scrollLeft() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func scrollMiddle() {
        scrollView.setContentOffset(CGPoint(x: screenSize.width, y: 0), animated: true)
        //Wenn view in der Mitte und Such Funktion an ist
        if(scrollView.contentOffset.x == screenSize.width && openSearchFunction){
            let input = searchView.searchTextField
            searchView.searchStartedEntering(input!)
            input?.becomeFirstResponder()
        }
    }
    
    @IBAction func scrollRight() {
        scrollView.setContentOffset(CGPoint(x: screenSize.width*2, y: 0), animated: true)
    }

}
