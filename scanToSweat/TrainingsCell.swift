//
//  TableViewCell.swift
//  scanToSweat
//
//  Created by Leon Schlote on 09.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class TrainingsCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var exerciseSets: [SetsPerExerciseData] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseSets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseSetsTrainingshistorieCell", for: indexPath) as? ExerciseSetsTrainingshistorieCell
        
        let currentSet = exerciseSets[indexPath.row]
       
           // print(currentSet)
            cell?.setLabels(
                uebung: currentSet.exercise.name!,
                geraet: "("+currentSet.exercise.geraeteId!+")",
                setCount: Int16(currentSet.setCount))
            // Configure the cell...
        
        return cell!
    }
    
    

    @IBOutlet private weak var datumLabel: UILabel!
    @IBOutlet private weak var trainingsTableView: UITableView!
    
    private let formatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        formatter.dateFormat = "dd. MMMM YYYY"
        trainingsTableView.delegate = self
        trainingsTableView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDatum(_ date: Date){
        //print(date)
        datumLabel.text = formatter.string(from: date)
    }
    
    func setSets(_ setsDict: [String: [Sets]]){
        //print(sets)
        
        
        self.exerciseSets = []
        
        for (_, setsArray) in setsDict{
            exerciseSets.append(SetsPerExerciseData(exercise:setsArray[0].machine!, setCount: setsArray.count))
        }
        
        self.tableView.reloadData()
        //datumLabel.text = formatter.string(from: date)
    }

}

struct SetsPerExerciseData {
    let exercise: Exercise
    let setCount: Int
}
