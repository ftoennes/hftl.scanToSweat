//
//  SearchViewController.swift
//  scanToSweat
//
//  Created by Leon Schlote on 08.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchResultTable: UITableView!
    @IBOutlet weak var flashLightButton: UIButton!
    @IBOutlet weak var hideKeyboardGestureRecognizer: UITapGestureRecognizer!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var dbExercises: [Exercise]?
    
    var searchFieldWidth: CGFloat!
    
    
    override func viewDidAppear(_ animated: Bool) {
        // deselect the selected row if any
        let selectedRow: IndexPath? = searchResultTable.indexPathForSelectedRow
        if let selectedRowNotNill = selectedRow {
            searchResultTable.deselectRow(at: selectedRowNotNill, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchResultTable.alpha = 0.0
        searchResultTable.isHidden = true
        
        
        // Do any additional setup after loading the view.
        
        searchFieldWidth = searchTextField.frame.width
        
        //layout
        //search field
      /*  searchTextField.layer.cornerRadius = 14.5
        searchTextField.layer.borderWidth = 2
        searchTextField.layer.borderColor = UIColor.clear.cgColor
        searchTextField.layer.masksToBounds = true
        */
        //search field icon
        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named: "search")
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        leftView.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        leftImageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        searchTextField.rightView = leftView
        searchTextField.rightViewMode = UITextFieldViewMode.always
        
        
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.searchTextField.frame.height))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = UITextFieldViewMode.always
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchStartedEntering(_ sender: UITextField) {
        //hideKeyboardGestureRecognizer.isEnabled = true
        
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.searchResultTable.isHidden = false
            self.searchResultTable.alpha = 1.0
            self.searchTextField.textAlignment = .left

            //self.searchResultTable.contentOffset.x = 0
            //self.searchResultTable.frame = CGRect.init(x: self.searchResultTable.frame width, y: 70, width: 0, height: self.view.frame.height/3)
        })
    }
    

    @IBAction func hideKeyboard(_ sender: Any) {
        dbExercises = []
        searchResultTable.reloadData()
        
        searchTextField.text = ""
        view.endEditing(true)
        hideKeyboardGestureRecognizer.isEnabled = false
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.searchTextField.textAlignment = .center
            self.searchResultTable.alpha = 0.0
            //self.searchResultTable.frame = CGRect.init(x: self.view.frame.width/2, y: 70, width: 0, height: self.view.frame.height/3)
        }, completion: { _ in
            
            self.searchResultTable.isHidden = true
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                      with event: UIEvent?) {
        if (!self.searchResultTable.isHidden) {
            hideKeyboard(self)
        }
    }
    
    @IBAction func searchValueChanged(_ sender: UITextField?) {
        let searchText = sender?.text!
        let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
        request.predicate = NSPredicate(format: "id CONTAINS[cd] %@ || name CONTAINS[cd] %@ || geraeteId CONTAINS[cd] %@ || muskel CONTAINS[cd] %@", searchText!, searchText!, searchText!, searchText!)
        do {
            dbExercises = try context.fetch(request)
        } catch {
            print("error while fetching")
        }
        
        
        searchResultTable.reloadData()
    }
    
    
    
    
    
    @IBAction func triggerFlashlight(_ sender: UIButton) {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                    flashLightButton.setImage(UIImage(named: "dunkel"), for: UIControlState())
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                        flashLightButton.setImage(UIImage(named: "eswerdelicht"), for: UIControlState())
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK:  - Tabellen-Funktionen
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (dbExercises != nil) {
            return (dbExercises?.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = UITableViewCell()
        cell.textLabel?.text = dbExercises?[indexPath.row].id
        
        return cell*/
        tableView.register(UINib(nibName: "MachineCell", bundle: nil), forCellReuseIdentifier: "MachineRow" + String(indexPath.row))
        let cell = tableView.dequeueReusableCell(withIdentifier: "MachineRow" + String(indexPath.row)) as? MachineCell
        cell?.idLabel.text = dbExercises?[indexPath.row].id
        cell?.label.text = dbExercises?[indexPath.row].name

        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewDidDisappear(false) //stop ocr
        
        DispatchQueue.main.async {
            let exercise = self.dbExercises![indexPath.row]
            
            let machineViewController = MachineViewController()
            machineViewController.machineId = exercise.id!
            self.present(machineViewController, animated: true, completion: nil)
            
            self.hideKeyboard(self)
        }
        
    }
    

}
