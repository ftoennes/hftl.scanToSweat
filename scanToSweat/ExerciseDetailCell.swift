//
//  ExerciseDetailCell.swift
//  scanToSweat
//
//  Created by Leon Schlote on 14.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class ExerciseDetailCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var datumLabel: UILabel!
    var setsArray: [Sets] = []
    
    @IBOutlet weak var setsTabelle: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setsTabelle.dataSource = self
        setsTabelle.delegate = self
        
        // Initialization code
        
    }
    
    //MARK: - Tabellenfunktionen
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseDetailSetTableViewCell", for: indexPath) as! ExerciseDetailSetTableViewCell
        
        cell.setNumberLabel.text = String(indexPath.row + 1)
        cell.repetitionsLabel.text = String(setsArray[indexPath.row].reps)
        cell.weightLabel.text = String(setsArray[indexPath.row].weight) + " kg"
                
        return cell
    }
    
    func setSets(_ newSets: [Sets]){
        setsArray = newSets
        setsTabelle.reloadData()
    }
    
    func setDatum(_ datum: Date){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd. MMMM YYYY"
        datumLabel.text = formatter.string(from: datum)
    }
    
    

}
