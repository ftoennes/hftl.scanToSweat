//
//  TableViewCell.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 10.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import CoreData

class MachineCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var label: UILabel!

}
