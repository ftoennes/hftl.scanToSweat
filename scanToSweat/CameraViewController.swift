//
//  CameraViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 11.10.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import AbbyyRtrSDK
import AVFoundation
import CoreData
import QuartzCore

class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, RTRTextCaptureServiceDelegate {
    
    
    
//#MARK: definitions
    private var session: AVCaptureSession?
    private let SessionPreset = AVCaptureSession.Preset.hd1280x720
    private var engine: RTREngine?
    private var previewLayer: AVCaptureVideoPreviewLayer?
    private var textCaptureService: RTRTextCaptureService?
    private var selectedRecognitionLanguages = Set(["German"])
    private var ImageBufferSize = CGSize(width: 720, height: 1280)
    private var maskView: UIView!
    public var isRunning = true
    public var currentImage: UIImage!
    private var machineId: String?
    private var mcFitIDPattern = "\\b([A-Z]{1})([0-9]{2})\\b"
    private var searchFieldWidth: CGFloat = 0.0 //workaround
    
    var allExerciseIds: Set<String> = Set()
    
    private let RTRTextRegionsLayerName = "TextRegionsLayer"
    
    var overlayView: UIImageView?

    
    //Core Data todo entfernen
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var previewView: PreviewView!
    @IBOutlet weak var blurEffectView: UIVisualEffectView!

//#MARK: default functs
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.updatePreviewLayerFrame()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //RTRSDK License
        let licensePath = (Bundle.main.bundlePath as NSString).appendingPathComponent("AbbyyRtrSdk.license")
        
        self.engine = RTREngine.sharedEngine(withLicense: NSData(contentsOfFile: licensePath) as Data!)
        assert(self.engine != nil)
        
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        switch status {
        case AVAuthorizationStatus.authorized:
            self.configureCompletionAccess(true)
            break
            
        case AVAuthorizationStatus.notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                DispatchQueue.main.async {
                    self.configureCompletionAccess(granted)
                }
            })
            break
            
        case AVAuthorizationStatus.restricted, AVAuthorizationStatus.denied:
            self.configureCompletionAccess(false)
            break
        }
        
        //prepare clear rectangle
        maskView = UIView(frame: view.bounds)
        maskView.clipsToBounds = true;
        maskView.backgroundColor = UIColor.clear
        let outerBezierPath = UIBezierPath.init(rect: blurEffectView.bounds)
        let rect = CGRect(x: maskView.bounds.width*0.1, y: maskView.bounds.height*0.25, width: maskView.bounds.width*0.8, height: maskView.bounds.height*0.5)
        self.selectedArea = rect
        let innerRectPath = UIBezierPath.init(rect: rect)
        
        
        outerBezierPath.append(innerRectPath)
        outerBezierPath.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.green.cgColor // any opaque color would work
        fillLayer.path = outerBezierPath.cgPath
        
        maskView.layer.addSublayer(fillLayer)
        
        //add Scanner Overlay
        overlayView = UIImageView(frame: CGRect(x: maskView.bounds.width*0.1-1.5, y: maskView.bounds.height*0.25-1.5, width: maskView.bounds.width*0.8+3.0, height: maskView.bounds.height*0.5+3.0))
        overlayView?.image = UIImage(named: "viewFinderOverlay")
        
        overlayView?.contentMode = .scaleToFill
        view.addSubview(overlayView!)
        
        
        //prepare OCR
        self.textCaptureService = self.engine?.createTextCaptureService(with: self)
        self.textCaptureService?.setRecognitionLanguages(selectedRecognitionLanguages)
          addGradientOverlay()
        
        
        
        //alle übungsids aus db fetchen
        let baseRequest: NSFetchRequest<Exercise> = Exercise.fetchRequest()
        baseRequest.propertiesToFetch = ["id"]
        
        let asyncRequest = NSAsynchronousFetchRequest(fetchRequest: baseRequest){ (fetchedTrainings) -> Void in
            DispatchQueue.main.async { () -> Void in
                let allExercises = fetchedTrainings.finalResult!
                
                for exercise in allExercises{
                    self.allExerciseIds.insert(exercise.id!)
                }
            }
        }
        
        //Datenbank request ausführen
        do {
            try context.execute(asyncRequest)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.session?.stopRunning()
        blurEffectView.mask = nil
        self.isRunning = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.session?.startRunning()
        }
        //add clear rectangle
        self.blurEffectView.mask = maskView;

        //start OCR
        updateAreaOfInterest()
        self.isRunning = true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        let wasRunning = self.isRunning
        self.textCaptureService?.stopTasks()
        self.clearScreenFromRegions()
        
        coordinator.animate(alongsideTransition: nil) { (context) in
            self.ImageBufferSize = CGSize(width:min(self.ImageBufferSize.width, self.ImageBufferSize.height),
                                          height:max(self.ImageBufferSize.width, self.ImageBufferSize.height))
            
            self.updateAreaOfInterest()
            self.isRunning = wasRunning;
        }
        
    }
    
    private var selectedArea: CGRect = CGRect.zero {
        didSet {
            self.previewView.selectedArea = selectedArea
        }
    }
    
    func configureCompletionAccess(_ accessGranted: Bool) {
        self.configureAVCaptureSession()
        self.configurePreviewLayer()
        self.session?.startRunning()
        

        NotificationCenter.default.addObserver(self, selector:#selector(CameraViewController.applicationDidEnterBackground(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(CameraViewController.applicationWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
    }
    
//#MARK: AV stuff
    
    private func configureAVCaptureSession() {
        self.session = AVCaptureSession()
        self.session?.sessionPreset = SessionPreset
        
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: device!)
            assert((self.session?.canAddInput(input))!, "impossible to add AVCaptureDeviceInput")
            self.session?.addInput(input)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        let videoDataOutput = AVCaptureVideoDataOutput()
        let videoDataOutputQueue = DispatchQueue(label: "videodataqueue", attributes: .concurrent)
        videoDataOutput.setSampleBufferDelegate(self, queue: videoDataOutputQueue)
        
        videoDataOutput.videoSettings = NSDictionary(object: Int(kCVPixelFormatType_32BGRA),
                                                     forKey: kCVPixelBufferPixelFormatTypeKey as! NSCopying) as! [String : Any]
        
        assert((self.session?.canAddOutput(videoDataOutput))!, "impossible to add AVCaptureVideoDataOutput")
        self.session?.addOutput(videoDataOutput)
        
        let connection = videoDataOutput.connection(with: AVMediaType.video)
        connection?.isEnabled = true
    }
    
    private func configurePreviewLayer() {
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session!)
        self.previewLayer?.backgroundColor = UIColor.black.cgColor
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resize
        let rootLayer = self.previewView.layer
        rootLayer .insertSublayer(self.previewLayer!, at: 0)
        
        self.updatePreviewLayerFrame()
    }
    
    private func updatePreviewLayerFrame() {
        self.previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        let viewBounds = self.view.bounds
        self.previewLayer?.frame = viewBounds
        
        self.selectedArea = viewBounds.insetBy(dx: viewBounds.width/8.0, dy: viewBounds.height/3.0)
        
        self.updateAreaOfInterest()
    }
    
    
//# MARK: - Notifications

    
    @objc func applicationDidEnterBackground(_ notification: NSNotification) {
        self.session?.stopRunning()
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        self.session?.startRunning()
    }
    
//#MARK: - OCR stuff
    
    //# MARK: - RTRRecognitionServiceDelegate
    func onBufferProcessed(with textLines: [RTRTextLine]!, resultStatus: RTRResultStabilityStatus) {
        self.processResult(textLines, resultStatus)
    }
    
    func recognitionProgress(_ progress: Int32, warningCode: RTRCallbackWarningCode) {
        switch warningCode {
        case RTRCallbackWarningCode.textTooSmall:
            print("Text is too small")
            return
        default:
            break
        }
    }
    
    func onError(_ error: Error!) {
        print(error.localizedDescription)
    }
    
    //# MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
    
    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if !self.isRunning {
            return
        }
        
        // Image is prepared
        connection.videoOrientation = AVCaptureVideoOrientation.portrait
        
        currentImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer)
        
       // let snapContainer: SnapContainerViewController = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! SnapContainerViewController
        
        DispatchQueue.main.async {
            UIGraphicsBeginImageContext(self.view.frame.size)
            self.currentImage.draw(in: self.view.bounds);
            self.currentImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext();
        }
        
        //snapContainer.scrollView.backgroundColor = UIColor.init(patternImage: currentImage)
        
        self.textCaptureService?.add(sampleBuffer)
    }
    
    //# MARK: - imageFromImageBuffer
    func imageFromSampleBuffer(sampleBuffer:CMSampleBuffer) -> UIImage? {
        if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
            CVPixelBufferLockBaseAddress(imageBuffer,CVPixelBufferLockFlags(rawValue: 0))
            let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
            let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
            let width = CVPixelBufferGetWidth(imageBuffer)
            let height = CVPixelBufferGetHeight(imageBuffer)
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let context = CGContext(data: baseAddress,width: width,height: height,bitsPerComponent: 8,bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
            
            let quartzImage = context!.makeImage()
            CVPixelBufferUnlockBaseAddress(imageBuffer,CVPixelBufferLockFlags(rawValue: 0))
            
            if let quartzImage = quartzImage {
                let image = UIImage(cgImage: quartzImage)
                return image
            }
        }
        return nil
    }

    //# MARK: - Drawing result
    
    private func processResult(_ areas: [RTRTextLine], _ mergeStatus:RTRResultStabilityStatus) {
        DispatchQueue.main.async {
            if !self.isRunning {
                return
            }
            
            if mergeStatus == RTRResultStabilityStatus.stable {
                self.handleScanningResult(areas)
            }
        }
    }
    
 
    
    /// Remove all visible regions
    private func clearScreenFromRegions() {
        // Get all visible regions
        let sublayers = self.previewView.layer.sublayers
        
        // Remove all layers with name - TextRegionsLayer
        for layer in sublayers! {
            if layer.name == RTRTextRegionsLayerName {
                layer.removeFromSuperlayer()
            }
        }
    }
   
    
    //#MARK: prepare OCR area
    
    private func updateAreaOfInterest() {
        let affineTransform = CGAffineTransform(scaleX: self.ImageBufferSize.width * 1.0 / self.previewView.frame.width, y: self.ImageBufferSize.height * 1.0 / self.previewView.frame.height)
        let selectedRect = self.selectedArea.applying(affineTransform)
        self.textCaptureService?.setAreaOfInterest(selectedRect)
        
    }
    

    //#MARK: McFit-ID-regex search
    //this method iterates through all lines and passes them to the regex func
    private func handleScanningResult(_ areas: [RTRTextLine]) {
        //MCFIT CHECK HERE
        var detectedIds: [String] = Array()
        for area in areas {
            let detectedId = self.detectMcFitId(_textLine: area)
            if (detectedId != nil) {
                detectedIds.append(detectedId!)
            }
        }
        if (detectedIds.count == 1) {
            let machineViewController = MachineViewController()
            machineViewController.machineId = detectedIds.first
            self.present(machineViewController, animated: true)
        } else if (detectedIds.count > 1) {
            self.viewDidDisappear(false)
            //setup multipleIdsViewController
            let multipleIdsViewController = MultipleIdsViewController(nibName: "MultipleIdsViewController", bundle:nil)
            multipleIdsViewController.exercises = detectedIds
            multipleIdsViewController.callingViewController = self
            
            let multipleIdsView = multipleIdsViewController.view
            multipleIdsView?.layer.masksToBounds = false
            multipleIdsView?.layer.cornerRadius = 15.0
            
            multipleIdsView?.frame = CGRect(x: self.view.frame.width + 5, y: self.view.frame.height/4, width: self.view.frame.width - 10, height: self.view.frame.height/4*2)
            
            let snapContainer: SnapContainerViewController = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! SnapContainerViewController
            multipleIdsView?.alpha = 0.0
            snapContainer.scrollView.addSubview(multipleIdsView!)
            UIView.animate(withDuration: 0.3, animations: {
                multipleIdsView?.alpha = 1.0
            })
            snapContainer.addChildViewController(multipleIdsViewController)
        }

    }
    
    //in textline
    private func detectMcFitId(_textLine: RTRTextLine) -> String? {
        do {
            let regex = try NSRegularExpression(pattern: mcFitIDPattern)
            let nsString = _textLine.text as NSString
            let results = regex.matches(in: _textLine.text, range: NSRange(location: 0, length: nsString.length))
            
            if (results.count == 1) {
               let result = nsString.substring(with: results[0].range)
                if allExerciseIds.contains(result){
                    return result
                }
            }
        } catch let error {
            print("invalid regex or not in db: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    func viewController(responder: UIResponder) -> UIViewController? {
        if let vc = responder as? UIViewController {
            return vc
        }
        
        if let next = responder.next {
            return viewController(responder: next)
        }
        
        return nil
    }

    func addGradientOverlay(){
        let gradientLayer: CAGradientLayer! = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        
        gradientLayer.colors = [
            UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.0),
            UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.1),
            UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
            ].map { $0.cgColor }
        gradientLayer.locations = [0.0, 0.8, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 1)
        
    }
}

