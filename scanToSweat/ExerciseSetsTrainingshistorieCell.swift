//
//  ExerciseSetsTrainingshistorieCell.swift
//  scanToSweat
//
//  Created by Leon Schlote on 09.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit

class ExerciseSetsTrainingshistorieCell: UITableViewCell {
   
    @IBOutlet weak var uebungsLabel: UILabel!
    @IBOutlet weak var geraeteLabel: UILabel!
    @IBOutlet weak var setCountLabel: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLabels(uebung: String, geraet: String, setCount: Int16){
        
        uebungsLabel.text = uebung
        uebungsLabel.sizeToFit()
        
        geraeteLabel.text = geraet
        
        setCountLabel.text = String(setCount)
        if(setCount == 1){
            setsLabel.text = "SET"
        }
    }
}
