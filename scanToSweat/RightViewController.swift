//
//  RightViewController.swift
//  scanToSweat
//
//  Created by Felix Martin Toenneßen on 07.12.17.
//  Copyright © 2017 Gruppe Fitness. All rights reserved.
//

import UIKit
import Charts
import CoreData

class RightViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var dates:[String] = []
    
    var containerView:SnapContainerViewController? = nil
    var trainingsScore:Int = 0
    var scoreChartData:[ScoreChartValue] = []
    
    let cells: [NavigationCell] = [
        NavigationCell(title: NSLocalizedString("Alle Übungen", comment: ""), segueID: "allExerciseSegue"),
        NavigationCell(title: NSLocalizedString("Trainingshistorie", comment: ""), segueID: "trainingsHistorieSegue")]
    
  //  var :[Sets] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    @IBOutlet weak var trainingsScoreLabel: UILabel!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var trainingsScoreChart: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView = self.parent?.parent as? SnapContainerViewController
        
        
        let maskPath = UIBezierPath(roundedRect: tableViewContainer.bounds,
                                    byRoundingCorners: [.bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        tableViewContainer.layer.mask = shape
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.containerView?.showIndicators()
        }
        
        // deselect the selected row if any
        let selectedRow: IndexPath? = tableView.indexPathForSelectedRow
        if let selectedRowNotNill = selectedRow {
            tableView.deselectRow(at: selectedRowNotNill, animated: true)
        }
        
    }
    
    //MARK: - Daten laden
    func reloadData(){
        
        let sort = NSSortDescriptor(key: "date", ascending: true)
        // Lade Trainings Asynchron
        let request:NSFetchRequest<Training> = Training.fetchRequest()
        request.sortDescriptors = [sort]
        request.fetchLimit = 7
        
        let trainingsRequest = NSAsynchronousFetchRequest(fetchRequest: request){ (fetchedTrainings) -> Void in
            DispatchQueue.main.async { () -> Void in
                self.scoreChartData = []
                for training in fetchedTrainings.finalResult!{
                    let date = training.date!
                    var score:Float = 0.0
                    
                    for set in training.sets?.allObjects as! [Sets]{
                        score +=  Float(set.reps) * set.weight
                    }
                    
                    let newSCV = ScoreChartValue(date: date, score: Int(score))
                    self.scoreChartData.append(newSCV)
                }
                
                if (self.scoreChartData.last == nil) {
                    self.trainingsScore = 0
                } else {
                    self.trainingsScore = self.scoreChartData.last!.score!
                }
                
                self.initTrainingsScoreLabel()
                
                self.initChartView()
                self.updateChartView()
            }
        }
        
        //Datenbank requests ausführen
        do {
            try context.execute(trainingsRequest)
            //try context.execute(setRequest)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
    }
    
    
    //MARK: - Trainingsscorechart-Section
    
    func updateChartView(){
        var trainingsScoreData = [ChartDataEntry]()
        dates.removeAll()
        
        var x = 0;
        var zeroValue: ChartDataEntry? = nil
        
        for scoreEntry in scoreChartData {
            //adds a x/y(0/0) value to array, if there is only one entry in trainingsScoreData
            //otherwise no line will be shown in the chart
            if scoreChartData.count == 1 {
                zeroValue = ChartDataEntry(x: Double(0), y: Double(0))
                trainingsScoreData.append(zeroValue!)
                dates.append("")
                x += 1
            }
            
            let df = DateFormatter()
            df.dateFormat = "dd.MM."
            if(x != 0){
                dates.append(df.string(from: scoreEntry.date))
            } else if (x == 0 && zeroValue == nil) {
                dates.append(df.string(from: scoreEntry.date))
            }
            
            let value = ChartDataEntry(x: Double(x), y: Double(scoreEntry.score))
            trainingsScoreData.append(value)
            
            x += 1
        }
        
        let line = LineChartDataSet(values: trainingsScoreData, label: NSLocalizedString("trainingsScore", comment: ""))
        line.colors = [UIColor(rgb: 0xCCC236)] //set to Green Color
        line.drawCirclesEnabled = false
        line.lineWidth = 4.0
        line.drawValuesEnabled = true
        
        
        let data = LineChartData()
        data.addDataSet(line)
        trainingsScoreChart.data = data
    }

    
    func initChartView(){
        trainingsScoreChart.drawGridBackgroundEnabled = false
        
        let xAxis = trainingsScoreChart.xAxis
        
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        xAxis.labelTextColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = true
        xAxis.gridColor = UIColor(patternImage: UIImage(named: "chartBG")!)
        xAxis.granularity = 1
        xAxis.valueFormatter = self
        xAxis.drawLabelsEnabled = true
        
        let yAxis = trainingsScoreChart.leftAxis
        yAxis.labelPosition = .outsideChart
        yAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        yAxis.labelTextColor = UIColor()
        yAxis.drawAxisLineEnabled = true
        yAxis.drawGridLinesEnabled = false
        yAxis.drawLabelsEnabled = false
        
        
        trainingsScoreChart.chartDescription?.enabled = false
        trainingsScoreChart.legend.enabled = false
        trainingsScoreChart.xAxis.avoidFirstLastClippingEnabled = false

        trainingsScoreChart.rightAxis.enabled = false
        
    }
    
    func initTrainingsScoreLabel(){
        
        if (trainingsScore < 10000){
            trainingsScoreLabel.text = String( Int(trainingsScore) )
        }else{
            trainingsScoreLabel.text = String(format: "%.1f",
                Float(trainingsScore) * 1.0 / 1000.0) + "k"
        }
    }
    
    
    //MARK: - Tabellenfunktionen
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = cells[indexPath.row].title
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        containerView?.hideIndicators()
        performSegue(withIdentifier: cells[indexPath.row].segueID, sender: self)
    }
}

//MARK: - Custom Types

struct NavigationCell {
    var title: String
    var segueID: String
}

struct ScoreChartValue{
    var date: Date!
    var score: Int!
}


extension RightViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
     //   print("value:", value)
      //  print("dates:", dates)
        if dates.count > Int(value) && value > 0{
            return dates[Int(value)]
        }else{
            return ""
        }
    }
}

