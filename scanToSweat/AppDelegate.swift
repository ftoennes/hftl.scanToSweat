//
//  AppDelegate.swift
//  SnapchatSwipeView
//
//  Created by Jake Spracher on 7/26/14.
//  Copyright (c) 2015 Jake Spracher. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var snapContainer: SnapContainerViewController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        // Override point for customization after application launch.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let left = storyboard.instantiateViewController(withIdentifier: "left")
        let middle = storyboard.instantiateViewController(withIdentifier: "middle")
        let right = storyboard.instantiateViewController(withIdentifier: "right")
        
        
        snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                    middleVC: middle as! SearchViewController,
                                                                    rightVC: right
                                                                )
        
        self.window?.rootViewController = snapContainer
        self.window?.makeKeyAndVisible()
        
        //database update - if internet connection allows it
        databaseUpdate()
        
     /*
        let context = self.persistentContainer.viewContext
       
       let request1: NSFetchRequest<Sets> = Sets.fetchRequest()
        if let result = try? context.fetch(request1) {
            for object in result {
                context.delete(object)
            }
        }
        
        let request2: NSFetchRequest<Training> = Training.fetchRequest()
        if let result = try? context.fetch(request2) {
            for object in result {
                context.delete(object)
            }
        }
        

        var exerciseA05: Exercise?
        var exerciseA10: Exercise?
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.mm.yyyy"
         
        do {
            let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
 
            request.predicate = NSPredicate(format: "id == %@", "A05")
            exerciseA05 = try context.fetch(request).first
            
            request.predicate = NSPredicate(format: "id == %@", "A20")
            exerciseA10 = try context.fetch(request).first
        } catch {
            print("fetching failed!")
        }
 
 */
        
      //  if (exerciseA05 != nil && exerciseA10 != nil){
         /*
            for i in 0...2000{
                var sets:[Sets] = []
                
                for _ in 0...5{
                    let set = Sets(context: context)
                    set.weight = 12.0
                    set.reps = 14
                    set.machine = exerciseA05
                    sets.append(set)
                }
                
                let training2 = Training(context: context)
                training2.date = formatter.date(from: "13.12."+String(2017+i))
                training2.sets = NSSet(array: sets)
                self.saveContext()
            }
            */
            
            
         /*   let set11 = Sets(context: context)
            set11.weight = 12.0
            set11.reps = 14
            set11.machine = exerciseA05
            
            let set12 = Sets(context: context)
            set12.weight = 12.0
            set12.reps = 12
            set12.machine = exerciseA05
            
            let set13 = Sets(context: context)
            set13.weight = 10.0
            set13.reps = 12
            set13.machine = exerciseA05
            
            let set14 = Sets(context: context)
            set14.weight = 12.0
            set14.reps = 12
            set14.machine = exerciseA10
            
            let set15 = Sets(context: context)
            set15.weight = 10.0
            set15.reps = 12
            set15.machine = exerciseA10
            
            let training1 = Training(context: context)
            training1.date = formatter.date(from: "14.12.2017")
            training1.sets = NSSet(array: [set11, set12, set13, set14, set15])
            self.saveContext()
            
            
            
            let set21 = Sets(context: context)
            set21.weight = 12.0
            set21.reps = 14
            set21.machine = exerciseA05
            
            let set22 = Sets(context: context)
            set22.weight = 12.0
            set22.reps = 12
            set22.machine = exerciseA05
            
            
            let training2 = Training(context: context)
            training2.date = formatter.date(from: "13.12.2017")
            training2.sets = NSSet(array: [set21, set22])
            self.saveContext()
            
            let set31 = Sets(context: context)
            set31.weight = 12.0
            set31.reps = 14
            set31.machine = exerciseA05
            
            let set32 = Sets(context: context)
            set32.weight = 12.0
            set32.reps = 12
            set32.machine = exerciseA05
            
            let set33 = Sets(context: context)
            set33.weight = 12.0
            set33.reps = 12
            set33.machine = exerciseA05
            
            
            let training3 = Training(context: context)
            training3.date = formatter.date(from: "12.12.2017")
            training3.sets = NSSet(array: [set31, set32, set33])
            self.saveContext()*/
 
    //    }
 
 
       /*
        for exerciseStruct in exercisesStruct {
            var dbExercise: [Exercise]?
            do {
                let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
                request.predicate = NSPredicate(format: "id == %@", exerciseStruct.id)
                dbExercise = try context.fetch(request)
            } catch {
                print("fetching failed!")
            }
            
            let context = self.persistentContainer.viewContext
            
            if dbExercise?.first?.id == exerciseStruct.id {
                //entry already exists
                print("Entry already exists - not saving:", exerciseStruct.id)
            } else {
                //save entry
                let exercise = Exercise(context: context) // Link
                exercise.id = exerciseStruct.id
                exercise.name = exerciseStruct.name
                exercise.bildId = exerciseStruct.bildId
                exercise.geraeteId = exerciseStruct.geraeteId
                exercise.muskel = exerciseStruct.muskel
                exercise.desc = exerciseStruct.desc
                print("Entry doesn't exist yet - saving:", exerciseStruct.id)
                self.saveContext()
            }
            
        }*/
        return true
    }
 
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "scanToSweat")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    // MARK: - Database Update
    func databaseUpdate() {
        let context = self.persistentContainer.viewContext

        
        let uri = "https://lawnetix.org:81/api/all_exercises_v2"
        let url = URL(string: uri)

        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            let nsUrlResponse = response as! HTTPURLResponse

            if nsUrlResponse.statusCode == 200 {

                var string = NSString.init(data: data!, encoding: String.Encoding.utf8.rawValue)?.replacingOccurrences(of: "geraete-id", with: "geraeteId")
                string = string?.replacingOccurrences(of: "bild-id", with: "bildId")
                let jsonData = string?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                let decoder = JSONDecoder()
                let exercisesStruct = try! decoder.decode([ExerciseStruct].self, from: jsonData!)

                for exerciseStruct in exercisesStruct {
                    var dbExercise: [Exercise]?
                    do {
                        let request: NSFetchRequest<Exercise> = Exercise.fetchRequest()
                        request.predicate = NSPredicate(format: "id == %@", exerciseStruct.id)
                        dbExercise = try context.fetch(request)
                    } catch {
                        print("fetching failed!")
                    }
                    
                    let context = self.persistentContainer.viewContext

                    if dbExercise?.first?.id == exerciseStruct.id {
                        //entry already exists
                        print("Entry already exists - not saving:", exerciseStruct.id)
                    } else {
                        //save entry
                        let exercise = Exercise(context: context) // Link
                        exercise.id = exerciseStruct.id
                        exercise.name = exerciseStruct.name
                        exercise.bildId = exerciseStruct.bildId
                        exercise.geraeteId = exerciseStruct.geraeteId
                        exercise.muskel = exerciseStruct.muskel
                        exercise.desc = exerciseStruct.desc
                        print("Entry doesn't exist yet - saving:", exerciseStruct.id)
                        self.saveContext()
                    }
                    
                }

            } else if nsUrlResponse.statusCode == 404 {

            }
        }

        task.resume()
    }
    
}

